# pwm-beeper


From Sunxi overlays:

```c
fragment@2 {
		target = <&pio>;
		__overlay__ {
			pwm0_pin: pwm0 {
				pins = "PA5";
				function = "pwm0";
			};
		};
	};

	fragment@3 {
		target = <&pwm>;
		__overlay__ {
			pinctrl-names = "default";
			pinctrl-0 = <&pwm0_pin>;
			status = "okay";
		};
	};
```

from :


https://github.com/torvalds/linux/blob/master/Documentation/devicetree/bindings/input/pwm-beeper.txt

```txt
 PWM beeper device tree bindings

Registers a PWM device as beeper.

Required properties:
- compatible: should be "pwm-beeper"
- pwms: phandle to the physical PWM device

Optional properties:
- amp-supply: phandle to a regulator that acts as an amplifier for the beeper
- beeper-hz:  bell frequency in Hz

Example:

beeper_amp: amplifier {
	compatible = "fixed-regulator";
	gpios = <&gpio0 1 GPIO_ACTIVE_HIGH>;
};

beeper {
	compatible = "pwm-beeper";
	pwms = <&pwm0>;
	amp-supply = <&beeper_amp>;
};
```

from linux5.4 `Documentation/devicetree/bindings/input/pwm-beeper.txt`

```
* PWM beeper device tree bindings

Registers a PWM device as beeper.

Required properties:
- compatible: should be "pwm-beeper"
- pwms: phandle to the physical PWM device

Optional properties:
- amp-supply: phandle to a regulator that acts as an amplifier for the beeper
- beeper-hz:  bell frequency in Hz

Example:

beeper_amp: amplifier {
	compatible = "fixed-regulator";
	gpios = <&gpio0 1 GPIO_ACTIVE_HIGH>;
};

beeper {
	compatible = "pwm-beeper";
	pwms = <&pwm0>;
	amp-supply = <&beeper_amp>;
};

```

need to consider:

https://patchwork.kernel.org/project/linux-input/patch/1460044644-12419-1-git-send-email-frieder.schrempf@exceet.de/

